import React, { Component } from 'react'
import PropTypes from 'prop-types'
import buildDebug from 'debug'
import { Switch, Route, withRouter } from 'react-router-dom'
import { observer } from 'mobx-react'

import RctConwayComponent from '@4geit/rct-conway-component'

import coverImage from './cover.png'

const debug = buildDebug('canercandan:conwaylife:app')

const HomeComponent = () => (
  <div>
    <RctConwayComponent
      backgroundImage={coverImage}
    />
  </div>
)

// @inject('swaggerClientStore', 'commonStore')
@withRouter
@observer
class App extends Component {
  static propTypes = {
    // commonStore: PropTypes.instanceOf(RctCommonStore).isRequired,
    // swaggerClientStore: PropTypes.instanceOf(RctSwaggerClientStore).isRequired,
    // eslint-disable-next-line react/forbid-prop-types, react/no-unused-prop-types
    location: PropTypes.object.isRequired,
  }

  // async componentWillMount() {
  //   await this.props.swaggerClientStore.buildClient()
  //   this.props.commonStore.setAppLoaded()
  // }

  render() {
    debug('render()')
    // const { isLoggedIn, appLoaded } = this.props.commonStore
    // if (!appLoaded) {
    //   return (<div>Loading...</div>)
    // }
    return (
      <div>
        <Switch>
          <Route path="/" component={HomeComponent} />
        </Switch>
      </div>
    )
  }
}

export default App
